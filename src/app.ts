// tslint:disable-next-line:no-var-requires
//const db = require("./db");
import * as Sequelize from "sequelize";
import { MongoConnection } from "./database/mongoConnection";
import { setTimeout } from "timers";
import { currentId } from "async_hooks";
const markets: any = ['USD-LTC', 'USD-NEO', 'USD-ETH', 'USD-XRP', 'USD-BTC', 'USD-OMG', 'USD-ADA', 'USD-DASH']
let i = 0;
let end = 8
let timeCount = 0;
//let rawCurrency: RawCurrency;
const connection: MongoConnection = new MongoConnection();
let test = (async () => {
    try {
        timeCount++;
        await connection.openDbConnection();
        let currentTimeStamp = new Date();
        let old1TimeStamp = new Date(+currentTimeStamp - 3600000);
        await insertHigh(currentTimeStamp.toISOString().substring(0, 23), old1TimeStamp.toISOString().substring(0, 23), "1hr");
        if (timeCount % 6 === 0) {
            let old6TimeStamp = new Date(+currentTimeStamp - 21600000);
            await insertHigh(currentTimeStamp.toISOString().substring(0, 23), old6TimeStamp.toISOString().substring(0, 23), "6hr");
            if (timeCount === 24) {
                let old24TimeStamp = new Date(+currentTimeStamp - 86400000);
                await insertHigh(currentTimeStamp.toISOString().substring(0, 23), old24TimeStamp.toISOString().substring(0, 23), "24hr");
                timeCount = 0;
            }
        }
        console.log(timeCount);
        console.log("Done")
    } catch (err) {

    }
    await timeOut();
    return test();
});

test();


async function timeOut() {
    var promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(true);
        }, 3600000)
    });
    return promise;
}
async function insertHigh(currentTimeStamp: string, oldTimeStamp: string, tableName: string) {
    for (i = 0; i < 8; i++) {
        try {
            let data = await connection.findbyTimeStamp(markets[i], currentTimeStamp, oldTimeStamp);
            await connection.insertDocument(data, markets[i] + "-" + tableName);
        }
        catch (err) {

        }
    }
}




