var MongoClient = require('mongodb').MongoClient;

export class MongoConnection {
    static connection_string: string = "mongodb://necatim:Aslan1905@tracker-shard-00-00-j5xfk.gcp.mongodb.net:27017,tracker-shard-00-01-j5xfk.gcp.mongodb.net:27017,tracker-shard-00-02-j5xfk.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Tracker-shard-0&authSource=admin&retryWrites=true";
    dbConnection: any = null;
    public async openDbConnection() {
        if (this.dbConnection == null) {
            try {
                var client = await MongoClient.connect(MongoConnection.connection_string);
                this.dbConnection = client.db('CRYvUSD');
                console.log("Connected correctly to MongoDB server.");
            }
            catch (err) {
                console.log("cannot connect to MongoDB server.", err);
            }
        }
    }

    public async insertDocument(document: any, collectionName: string): Promise<any> {
        if (this.dbConnection) {
            try {
                let respone = await this.dbConnection.collection(collectionName).insert(document);
            }
            catch (err) {
                return Promise.reject(err + "Could not insert");
            }
        }
    }

    public async findbyTimeStamp(collectionName: string, currentTimeStamp, previousTimeStamp) {
        if (this.dbConnection) {
            try {
                const result = await this.dbConnection.collection(collectionName)
                    .find({ "TimeStamp": { $gte: previousTimeStamp, $lt: currentTimeStamp } }).sort({ "Last": -1 }).limit(1).toArray();
                return Promise.resolve(result);
            }
            catch (err) {
                return Promise.reject(err +"Failed get fucked");
            }
        }
    }
}

