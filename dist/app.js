"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoConnection_1 = require("./database/mongoConnection");
const timers_1 = require("timers");
const markets = ['USD-LTC', 'USD-NEO', 'USD-ETH', 'USD-XRP', 'USD-BTC', 'USD-OMG', 'USD-ADA', 'USD-DASH'];
let i = 0;
let end = 8;
let timeCount = 0;
//let rawCurrency: RawCurrency;
const connection = new mongoConnection_1.MongoConnection();
let test = (() => __awaiter(this, void 0, void 0, function* () {
    try {
        timeCount++;
        yield connection.openDbConnection();
        let currentTimeStamp = new Date();
        let old1TimeStamp = new Date(+currentTimeStamp - 3600000);
        console.log(currentTimeStamp);
        console.log(old1TimeStamp);
        yield insertHigh(currentTimeStamp.toISOString().substring(0, 23), old1TimeStamp.toISOString().substring(0, 23), "1hr");
        if (timeCount % 6 === 0) {
            let old6TimeStamp = new Date(+currentTimeStamp - 21600000);
            yield insertHigh(currentTimeStamp.toISOString().substring(0, 23), old6TimeStamp.toISOString().substring(0, 23), "6hr");
            if (timeCount === 24) {
                let old24TimeStamp = new Date(+currentTimeStamp - 86400000);
                yield insertHigh(currentTimeStamp.toISOString().substring(0, 23), old24TimeStamp.toISOString().substring(0, 23), "24hr");
                i = 0;
            }
        }
        console.log(timeCount);
        console.log("Done");
    }
    catch (err) {
    }
    yield timeOut();
    return test();
}));
test();
function timeOut() {
    return __awaiter(this, void 0, void 0, function* () {
        var promise = new Promise((resolve, reject) => {
            timers_1.setTimeout(() => {
                resolve(true);
            }, 100);
        });
        return promise;
    });
}
function insertHigh(currentTimeStamp, oldTimeStamp, tableName) {
    return __awaiter(this, void 0, void 0, function* () {
        for (i = 0; i < 8; i++) {
            try {
                let data = yield connection.findbyTimeStamp(markets[i], currentTimeStamp, oldTimeStamp);
                yield connection.insertDocument(data, markets[i] + "-" + tableName);
            }
            catch (err) {
            }
        }
    });
}
//# sourceMappingURL=app.js.map