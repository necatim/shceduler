"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var MongoClient = require('mongodb').MongoClient;
class MongoConnection {
    constructor() {
        this.dbConnection = null;
    }
    openDbConnection() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.dbConnection == null) {
                try {
                    var client = yield MongoClient.connect(MongoConnection.connection_string);
                    this.dbConnection = client.db('CRYvUSD');
                    console.log("Connected correctly to MongoDB server.");
                }
                catch (err) {
                    console.log("cannot connect to MongoDB server.", err);
                }
            }
        });
    }
    insertDocument(document, collectionName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.dbConnection) {
                try {
                    let respone = yield this.dbConnection.collection(collectionName).insert(document);
                }
                catch (err) {
                    return Promise.reject(err + "Could not insert");
                }
            }
        });
    }
    findbyTimeStamp(collectionName, currentTimeStamp, previousTimeStamp) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.dbConnection) {
                try {
                    const result = yield this.dbConnection.collection(collectionName)
                        .find({ "TimeStamp": { $gte: previousTimeStamp, $lt: currentTimeStamp } }).sort({ "Last": -1 }).limit(1).toArray();
                    return Promise.resolve(result);
                }
                catch (err) {
                    return Promise.reject(err + "Failed get fucked");
                }
            }
        });
    }
}
MongoConnection.connection_string = "mongodb://necatim:Aslan1905@tracker-shard-00-00-j5xfk.gcp.mongodb.net:27017,tracker-shard-00-01-j5xfk.gcp.mongodb.net:27017,tracker-shard-00-02-j5xfk.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Tracker-shard-0&authSource=admin&retryWrites=true";
exports.MongoConnection = MongoConnection;
//# sourceMappingURL=mongoConnection.js.map